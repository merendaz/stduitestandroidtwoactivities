/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.twoactivities;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.android.twoactivities", appContext.getPackageName());
    }


    // Put the name of the screen you want android to start on
    @Rule
    public ActivityTestRule activityRule =
            new ActivityTestRule<>(MainActivity.class);


    // TC1: When you click on Button, it goes to next page
    @Test
    public void testGoingToNextPage() throws InterruptedException {
        // 1. find the button (button_main)
        // 2. click on the button
        onView(withId(R.id.button_main)).perform(click());

        Thread.sleep(5000);


        // 3. check that page 2 is showing a Textview with id = text_header
        onView(withId(R.id.text_header)).check(matches(isDisplayed()));

        Thread.sleep(3000);


        // 4. done - go back to previous page
        onView(withId(R.id.button_second)).perform(click());


        Thread.sleep(3000);

    }




    // TC2: When you type nonsense into the text box,
    // it appears on page 2
    @Test
    public void testInputBox() throws InterruptedException {
        // 1. Find the text box
        // 2. Type something into the text box
        onView(withId(R.id.editText_main))
                .perform(typeText("HELLO JENELLE"));

        Thread.sleep(2000);

        // 3. Go to the next page
        //  - push the button
        onView(withId(R.id.button_main))
                .perform(click());

        Thread.sleep(2000);

        // 4. Check if the message is correct
        //  - expectedOutput = whatever you typed into page 1
        String expectedOutput = "HELLO JENELLE";
        onView(withId(R.id.text_message))
                .check(matches(withText(expectedOutput)));

        Thread.sleep(2000);

    }





}
